var server = require('./server');
var ds = server.dataSources.postgresql;
var lbTables = ['User', 'AccessToken', 'ACL', 'RoleMapping', 'Role', 'projects', 'jobs'];

ds.automigrate(lbTables, function(er) {
  if (er) throw er;
  console.log('Loopback tables [' - lbTables - '] created in ', ds.adapter.name);
  ds.disconnect();
});
